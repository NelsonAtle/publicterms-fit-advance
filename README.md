# TERMINOS Y CONDICIONES
## El usuario se compromete a utilizar la aplicación de conformidad con la Ley, las presentes Condiciones de Uso, y demás avisos, reglamentos de uso e instrucciones puestos en su conocimiento, así como con la moral y las buenas costumbres generalmente aceptadas y el orden público.

## El usuario ha de registrarse en la aplicación para usar sus servicios con datos correctos, reales y  proporcionar el número de teléfono y correo electrónico actuales, así como todos los datos biomédicos que la aplicación le solicite. 

## Los datos serán utilizados por el titular de la aplicación para fundamentar y establecer una rutina correcta de ejercicio y acondicionamiento físico adecuado para el usuario, por lo que el usuario autoriza de forma expresa la utilización de todos los datos suministrados a la aplicación. El titular no podrá utilizar los datos que el usuario le proporcione, con un fin diferente al autorizado.

## Se señala que los datos recopilamos de nuestros usuarios en el curso normal de la prestación de nuestros servicios, son resguardados y no se comparten con ninguna otra persona o aplicación, si la autorización expresa del usuario.

## Que el titular de la aplicación no se hace responsable por el uso inadecuado o que no se ajuste al presente aviso legal por parte del usuario o si el mismo no cumple con lo estipulado con el presente aviso legal. 

## La inscripción y uso de la aplicación por parte del usuario, dan por aceptadas de forma expresa las reglas y condiciones así como la obligación del cumplimiento de las mismas.

## Que al tener la aplicación, como objetivo, el lograr que el usuario realice actividad física y tenga un acondicionamiento físico adecuado, al registrarse el usuario, debe tener las condiciones físicas aptas para realizar las actividad y ejercicios que la aplicación, con fundamento  en los datos aportados por el mismo usuario, le indiquen, por lo que es requisito para el uso adecuado y correcto de la aplicación,  que el usuario cuente con el aval médico expreso que le permita la correcta utilización de la aplicación. 

## El usuario se obliga a usar los contenidos ofrecidos en la aplicación de forma diligente, correcta y lícita y, en particular, se compromete a abstenerse de utilizar los contenidos de forma, con fines o efectos contrarios a la ley, a la moral y a las buenas costumbres generalmente aceptadas o al orden público; reproducir o copiar, distribuir, permitir el acceso del público a través de cualquier modalidad de comunicación pública, transformar o modificar los contenidos, a menos que se cuente con la autorización del titular de los correspondientes derechos o ello resulte legalmente permitido; suprimir, eludir o manipular el copyright y demás datos identificativos. 

## La utilización de la aplicación por un tercero, le atribuye la condición de usuario y supone la aceptación plena por dicho usuario, de todas y cada una de las condiciones que se incorporan en el presente aviso legal.

## Se acepta recibir mensajes de texto y llamadas telefónicas por parte de la aplicación o proveedores externos.

## Para registrarse y poder utilizarlo se ha de tener por lo menos _ años de edad (o la edad mínima requerida en cada país para tener autorización sin aprobación de los padres).

## El usuario es el único responsable frente a cualquier reclamación o acción legal, judicial o extrajudicial, iniciada por terceras personas contra la aplicación basada en la utilización por el usuario de la aplicación.

## En su caso, el usuario asumirá los gastos, costos e indemnizaciones que sean provocados con motivo de acciones legales por la utilización incorrecta de la aplicación.

## Se otorga al usuario una licencia de uso de la aplicación limitada, revocable, no exclusiva, que no puede sublicenciarse ni transferirse para usar los servicios, sujeta a sus términos y conforme a ellas.

## Esta licencia se otorga con el único propósito de permitir usar los servicios de la aplicación en la manera estipulada en los términos y en este aviso legal. 

## No se otorgarán licencias o derechos de manera implícita ni de ninguna otra manera, excepto por las licencias y los derechos expresamente otorgados al usuario.

## El Usuario se compromete a no introducir programas, virus o cualquier dispositivo que causen o sean susceptibles de causar cualquier tipo de alteración en los sistemas informáticos de la aplicación.

## El titular y operador de la aplicación está facultada para suspender temporalmente, y sin previo aviso, la accesibilidad a la aplicación con motivo de operaciones de desarrollo, mantenimiento, reparación, actualización o mejora.

## En el supuesto de que la aplicación pudiera contener vínculos o enlaces con otros portales o sitios no gestionados por el titular y operador de la aplicación,  no existirá responsabilidad de ningún tipo por  el contenido de los mismos.

## Todos los contenidos de la aplicación,  salvo que se indique lo contrario, son titularidad exclusiva del titular, con carácter enunciativo, que no limitativo, el diseño gráfico, código fuente, logos, textos, gráficos, ilustraciones, fotografías, y demás elementos que aparecen en la aplicación.

## Igualmente, todos los nombres comerciales, marcas o signos distintivos de cualquier clase contenidos en la aplicación están protegidos por la Ley.

## El titular de la aplicación concede ningún tipo de licencia o autorización de uso personal al usuario sobre sus derechos de propiedad intelectual e industrial o sobre cualquier otro derecho relacionado con la aplicación y los servicios ofrecidos en la misma.

## El usuario reconoce que la reproducción, distribución, comercialización, transformación, y en general, cualquier otra forma de explotación, por cualquier procedimiento, de todo o parte de los contenidos de la aplicación constituye una infracción de los derechos de propiedad intelectual del titular de los mismos.

## El usuario, única y exclusivamente, puede utilizar el material que aparezca en la aplicación para su uso personal y privado, quedando prohibido su uso con fines comerciales o para incurrir en actividades ilícitas.

## El titular u operario de la aplicación se reserva el derecho a efectuar, sin previo aviso, las modificaciones que consideren oportunas en la aplicación, pudiendo cambiar, suprimir o añadir tanto los contenidos, materiales y servicios que presta, como la forma en la que estos aparezcan presentados.

## El titular de la aplicación podrá sustituir, en cualquier momento, por motivos técnicos o por cambios en la prestación del servicio o en la normativa, así como modificaciones por decisiones corporativas estratégicas, las condiciones y la política de privacidad que, según los casos, sustituirán, complementaran o  modificaran las condiciones y política de privacidad de la aplicación.
